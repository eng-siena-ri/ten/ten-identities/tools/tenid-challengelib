package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/hex"
	"fmt"
	. "gitlab.com/eng-siena-ri/ten/ten-identities/tools/tenid-challengelib"
	"math/big"
)

func main() {
	Initialize()
	var addr = "ddsdf4555454df"
	random, err := CreateRandom(addr)
	if err != nil {
		fmt.Errorf("Error in Creation of Random", err)
		return
	}
	fmt.Println("This is the random generated " + random)
	challenge := GetChallenge(addr)
	fmt.Println(challenge)
	createRandom, err := CreateRandom(addr)
	if err != nil {
		fmt.Errorf("Error in Creation of Random", err)
		return
	}
	fmt.Println("This is the otp generated: " + createRandom)
}

func hexToPublicKey(xHex string, yHex string) (pb *ecdsa.PublicKey) {
	xBytes, _ := hex.DecodeString(xHex)
	x := new(big.Int)
	x.SetBytes(xBytes)
	yBytes, _ := hex.DecodeString(yHex)
	y := new(big.Int)
	y.SetBytes(yBytes)
	pub := new(ecdsa.PublicKey)
	pub.X = x
	pub.Y = y
	pub.Curve = elliptic.P256()
	return pub
}
