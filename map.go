package tenid_challengelib

import (
	"fmt"
	"sync"
	"time"
)

type ChallengeMap struct {
	_map map[string]Challenge
	lock sync.RWMutex
}

// New creates a new ChallengeMap
func (m *ChallengeMap) New() *ChallengeMap {
	if m.IsEmpty() {
		m._map = make(map[string]Challenge)
	}
	return m
}

// Put an Challenge to the end of the map
func (m *ChallengeMap) Put(addr string, t Challenge) {
	m.lock.Lock()
	m._map[addr] = t
	m.lock.Unlock()
}

// Get a Challenge from the start of the map
func (m *ChallengeMap) Get(addr string) *Challenge {
	m.lock.Lock()
	challenge := m._map[addr]
	m.lock.Unlock()
	return &challenge
}

// Remove a Challenge in the map
func (m *ChallengeMap) Remove(addr string) {
	m.lock.RLock()
	delete(m._map, addr)
	m.lock.RUnlock()
}

// IsEmpty returns true if the queue is empty
func (m *ChallengeMap) IsEmpty() bool {
	return len(m._map) == 0
}

// Size returns the number of Items in the map
func (m *ChallengeMap) Size() int {
	return len(m._map)
}

func (m *ChallengeMap) isExpired(addr string) bool {
	timestamp := m._map[addr].Timestamp
	t1, error := time.Parse(dateFormat, timestamp)
	if error != nil {
		fmt.Errorf(ERROR_TIMESTAMP_CONVERSION, error)
		return false
	}
	if t1.Add(time.Duration(ChallengeDuration)).Before(time.Now()) {
		return true
	}
	return false
}
