package tenid_challengelib

const CHALLENGE_QUEUE_LIB = "ChallengeQueueLib - "

const MESSAGE_FIRST_ELEMENT_TO_ADD = CHALLENGE_QUEUE_LIB + "First element to add in queue"
const ERROR_TIMESTAMP_CONVERSION = CHALLENGE_QUEUE_LIB + "Error in conversion of Timestamp"
const ERROR_QUEUE_DELAY = CHALLENGE_QUEUE_LIB + "milliseconds before requesting a new OTP"
const ERROR_MAP_EXPIRED_VERIFY = CHALLENGE_QUEUE_LIB + "Errror in converting Timestamp to date"
