package tenid_challengelib

import (
	"crypto/md5"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"log"
	//"github.com/pquerna/otp/totp"
)

/*
// Generates Passcode using a UTF-8 (not base32) secret and custom paramters
func generatePassCode(utf8string string) string {
	secret := base32.StdEncoding.EncodeToString([]byte(utf8string))
	passcode, err := totp.GenerateCodeCustom(secret, time.Now(), totp.ValidateOpts{
		Period:    30,
		Skew:      1,
		Digits:    otp.DigitsEight,
		Algorithm: otp.AlgorithmSHA512,
	})
	if err != nil {
		panic(err)
	}
	return passcode
}
*/

// Generates Passcode using a UTF-8 (not base32) secret and custom parameters using SHA512 Algorithm
func generatePassCodeSHA512(utf8string string) string {
	h := sha512.New()
	h.Write([]byte(utf8string))
	bs := h.Sum(nil)
	sh := string(fmt.Sprintf("%x\n", bs))
	return sh
}

// Generates Passcode using a UTF-8 (not base32) secret and custom parameters using SHA256 Algorithm
func generatePassCodeSHA256(utf8string string) string {
	h := sha256.New()
	h.Write([]byte(utf8string))
	bs := h.Sum(nil)
	sh := string(fmt.Sprintf("%x\n", bs))
	return sh
}

// GenerateToken returns a unique token based on the provided string
func generateToken(utf8string string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(utf8string), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Random generated:", string(hash))
	hasher := md5.New()
	hasher.Write(hash)
	return hex.EncodeToString(hasher.Sum(nil))
}
