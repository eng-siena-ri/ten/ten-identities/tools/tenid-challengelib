package tenid_challengelib

import (
	"fmt"
	"time"
)

func ExecuteRemoveExpired() {
	go func() {
		for true {
			fmt.Println("Starting Routine for Removing Expired Challenges at " + time.Now().Format(time.RFC3339))
			removeExpiredChallenges()
			time.Sleep(time.Duration(ExecutionPeriod) * time.Millisecond)
		}
	}()
}
