package tenid_challengelib

import (
	"errors"
	"fmt"
	"time"
)

var challengeQueue ChallengeMap
var QueueDelay int64 = 500  //milliseconds
var ChallengeDuration = 500 //milliseconds
const dateFormat = time.RFC3339

var ExecutionPeriod int64 = 1000 * 60 //1 minute

// Challenge the type of the queue
type Challenge struct {
	Random    string
	Timestamp string
	Addr      string
}

// Entrypoint for library to use in init method of Chaincode
func Initialize() {
	doInit()
}

func InitializeWithParameters(queueDelay int64, challengeDuration int, executionPeriod int64) {
	QueueDelay = queueDelay
	ChallengeDuration = challengeDuration
	ExecutionPeriod = executionPeriod
	doInit()
}

func doInit() {
	initMap()
	ExecuteRemoveExpired()
}

// Function to create Random using a String Address
func CreateRandom(addr string) (string, error) {
	_, error := checkFlooding(addr)
	if error != nil {
		return "", error
	}
	random := generateToken(addr)
	challenge := createChallenge(addr, random)
	challengeQueue.Put(addr, *challenge)
	return random, nil
}

func checkFlooding(addr string) (string, error) {
	//Check against otp flooding
	challengeElement := GetChallenge(addr)
	if challengeElement == nil ||
		challengeElement.Timestamp == "" ||
		challengeElement.Random == "" {
		fmt.Println(MESSAGE_FIRST_ELEMENT_TO_ADD)
		return "", nil
	}
	fmt.Println(CHALLENGE_QUEUE_LIB + "Challenge retrieved is: ")
	fmt.Println(challengeElement)
	timestamp, err := time.Parse(dateFormat, challengeElement.Timestamp)
	if err != nil {
		fmt.Errorf(ERROR_TIMESTAMP_CONVERSION)
		return "", errors.New(ERROR_TIMESTAMP_CONVERSION)
	}
	diff := int64(time.Since(timestamp) / time.Millisecond)
	if diff < QueueDelay {
		fmt.Errorf("Wait " + string(QueueDelay) + ERROR_QUEUE_DELAY)
		return "", errors.New("Wait " + string(QueueDelay) + ERROR_QUEUE_DELAY)
	}
	return "", nil
}

func initMap() {
	challengeQueue.New()
}

func GetChallenge(addr string) *Challenge {
	if !challengeQueue.IsEmpty() {
		//removeExpiredChallenges()
		var challenge = challengeQueue.Get(addr)
		challengeQueue.Remove(addr)
		return challenge
	}
	return nil
}

func createChallenge(addr string, random string) *Challenge {
	// Time Format here -> https://flaviocopes.com/go-date-time-format/
	var challenge = Challenge{Random: random, Timestamp: time.Now().Format(dateFormat), Addr: addr}
	return &challenge
}

func removeExpiredChallenges() {
	if challengeQueue.IsEmpty() {
		return
	}
	for addr, value := range challengeQueue._map {
		fmt.Println(addr, value)
		if challengeQueue.isExpired(addr) {
			challengeQueue.Remove(addr)
		}
	}

}
