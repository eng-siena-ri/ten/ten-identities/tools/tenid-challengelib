package tenid_challengelib

import (
	"testing"
)

func testChallengeQueue(t *testing.T) {
	Initialize()
	var addr = "ddsdf4555454df"
	random, err := CreateRandom(addr)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("This is the Random created" + random)
	challengeQueue := GetChallenge(addr)
	passCodeRetrieved := challengeQueue.Random
	if random != passCodeRetrieved {
		t.Errorf("Expected %s, got %s", random, passCodeRetrieved)
	}
	t.Log("Works")
}

func testFlooding(t *testing.T) {
	InitializeWithParameters(1000, 500, 1000*60)
	var addr = "ddsdf4555454df"
	random, err := CreateRandom(addr)
	if err != nil {
		t.Fatal(err)
	}
	createdRandom, err := CreateRandom(addr)
	if err != nil {
		t.Fatal(err)
	}
	if random != createdRandom {
		t.Errorf("Expected %s, got %s", random, createdRandom)
	}
	t.Log("Works")
}

func testFloodingWithError(t *testing.T) {
	InitializeWithParameters(1, 500, 1000*60)
	var addr = "ddsdf4555454df"
	_, err := CreateRandom(addr)
	if err != nil {
		t.Fatal(err)
	}
	_, err = CreateRandom(addr)
	if err != nil {
		t.Log("Works")
	}
	t.Fail()
}
